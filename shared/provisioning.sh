#!/usr/bin/env bash

echo "Provisioning virtual machine ..."

if ! ansible --version | grep ansible;
then
    echo "-> Installing Ansible"
    # Add Ansible Repository
    sudo apt-add-repository ppa:ansible/ansible
    sudo apt-get update

    # Install Ansible
    sudo apt install ansible -y
    # Install Python
    sudo apt-get install python -y

    # Add SSH key
    cat /ansible/files/authorized_keys >> /home/vagrant/.ssh/authorized_keys
else
        echo "-> Ansible already Installed!"
fi

# Execute Ansible
echo "-> Execute Ansible"
ansible-playbook /ansible/playbook_development.yml -i /ansible/inventories/hosts --connection=local

# Install Ansistrano
echo "-> Install Ansistrano"
ansible-galaxy install ansistrano.deploy ansistrano.rollback

# Add host
echo "-> Add host"
cp /var/www/parkings-invoices-host.conf /etc/apache2/sites-available/
cd /etc/apache2/sites-enabled
ln -s /etc/apache2/sites-available/parkings-invoices-host.conf .
rm -r /etc/apache2/sites-available/000-default.conf
rm -r /etc/apache2/sites-enabled/000-default.conf

# Install toolchain
echo "-> Install toolchain"
wget -nv -O/usr/local/bin/composer https://getcomposer.org/composer.phar && chmod +x /usr/local/bin/composer -vv
wget -nv -O/usr/local/bin/phpcs https://squizlabs.github.io/PHP_CodeSniffer/phpcs.phar && chmod +x /usr/local/bin/phpcs -vv
wget -nv -O/usr/local/bin/phploc https://phar.phpunit.de/phploc.phar && chmod +x /usr/local/bin/phploc -vv
wget -nv -O/usr/local/bin/pdepend http://static.pdepend.org/php/latest/pdepend.phar && chmod +x /usr/local/bin/pdepend -vv
wget -nv -O/usr/local/bin/phpmd http://static.phpmd.org/php/latest/phpmd.phar && chmod +x /usr/local/bin/phpmd -vv
wget -nv -O/usr/local/bin/phpcs https://squizlabs.github.io/PHP_CodeSniffer/phpcs.phar && chmod +x /usr/local/bin/phpcs -vv
wget -nv -O/usr/local/bin/phpcbf https://squizlabs.github.io/PHP_CodeSniffer/phpcbf.phar && chmod +x /usr/local/bin/phpcbf -vv

# Update repositories
echo "-> Update repositories"
apt-get update

# PHP upgrade
echo "-> PHP Upgrade"
apt-get install -y python-software-properties
add-apt-repository ppa:ondrej/php
apt-get update
apt-get install -y php7.2

# PHP modules and Xdebug
echo "-> PHP modules and Xdebug"
apt-get install -y php7.2-bcmath php7.2-bz2 php7.2-cgi php7.2-cli php7.2-common php7.2-curl php7.2-dba php7.2-dev php7.2-enchant php7.2-fpm php7.2-gd php7.2-gmp php7.2-imap php7.2-interbase php7.2-intl php7.2-json php7.2-ldap php7.2-mbstring php7.2-mysql php7.2-odbc php7.2-opcache php7.2-pgsql php7.2-phpdbg php7.2-pspell php7.2-readline php7.2-recode php7.2-snmp php7.2-soap php7.2-sqlite3 php7.2-sybase php7.2-tidy php7.2-xml php7.2-xmlrpc php7.2-xsl php7.2-zip php7.2-simplexml

# Switch PHP version
echo "-> Switch PHP version"
a2dismod php7.0
a2enmod php7.2

# Edit Apache envvars
echo "-> Edit Apache envvars"
sed -i 's/export APACHE_RUN_USER=www-data/export APACHE_RUN_USER=vagrant/g' /etc/apache2/envvars
sed -i 's/export APACHE_RUN_GROUP=www-data/export APACHE_RUN_GROUP=vagrant/g' /etc/apache2/envvars

# Restart Apache
echo "-> Restart Apache"
service apache2 restart

# Install Laravel
echo "-> Install Laravel"
composer global require laravel/installer
service apache2 restart