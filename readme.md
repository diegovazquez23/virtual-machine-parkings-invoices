## Trabajo Final de Master - Parkimeter - Virtual Machine

- Clonar este repositorio para crear la máquina virtual:

		git clone https://bitbucket.org/diegovazquez23/virtual-machine-parkings-invoices.git

- Moverse al nuevo directorio:

		cd virtual-machine-parkings-invoices

- Subir la máquina:

		vagrant up
	
- En caso de recibir un error de Virtual Box Guest, ejecutar los siguientes comandos:
		
		vagrant plugin repair
		vagrant plugin expunge --reinstall
		vagrant reload

- Conectarse a la VM vía SSH:

		vagrant ssh
		
